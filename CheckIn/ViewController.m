//
//  ViewController.m
//  CheckIn
//
//  Created by Miniplayground on 5/14/56 BE.
//  Copyright (c) 2556 Fireoneone. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <UITextFieldDelegate,CLLocationManagerDelegate> {
    
    CLLocationManager *_locationManager;
    CLGeocoder *_geocoder;
    //converting between a GPS coordinate and the user-readable address of that coordinate.
    //returned by CLGeocoder is saved in a CLPlacemark object.
    CLPlacemark *_placemark;
    
    
    __weak IBOutlet UILabel *_latitudeLabel;
    __weak IBOutlet UILabel *_longitudeLabel;
    __weak IBOutlet UILabel *_resultLabel;
    __weak IBOutlet UILabel *_addressLabel;
    
    double officeLatiitude;
    double officeLongitude;
    
    
    
}
- (IBAction)checkInButtonDidTouch:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Fire One One Location
    officeLatiitude = 13.745198;
    officeLongitude = 100.540774;
    
    _locationManager = [[CLLocationManager alloc] init];
    _geocoder = [[CLGeocoder alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkInButtonDidTouch:(id)sender
{
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    [_locationManager startUpdatingLocation];


    
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error"
                               message:@"Failed to Get Your Location"
                               delegate:nil cancelButtonTitle:@"OK"
                               otherButtonTitles:nil];
    [errorAlert show];

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        _longitudeLabel.text = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.longitude];
        _latitudeLabel.text = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.latitude];
    }
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [_geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            _placemark = [placemarks lastObject];
            _addressLabel.text = [NSString stringWithFormat:@"%@\n%@ %@\n%@ %@\n%@\n%@",
                                  _placemark.name,
                                 _placemark.subThoroughfare, _placemark.thoroughfare,
                                 _placemark.postalCode, _placemark.locality,
                                 _placemark.administrativeArea,
                                 _placemark.country];
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
   
    NSLog(@"lat %f:%f",currentLocation.coordinate.latitude,officeLatiitude);
    NSLog(@"long %f:%f",currentLocation.coordinate.longitude,officeLongitude);
    
    if (currentLocation.coordinate.latitude == officeLatiitude && currentLocation.coordinate.longitude == officeLongitude) {
        _resultLabel.text = @"OK";
        _resultLabel.backgroundColor = [UIColor greenColor];
        
        
    } else {
        _resultLabel.text = @"FAIL";
        _resultLabel.backgroundColor = [UIColor redColor];
        
    }
    [self performSelector:@selector(stopUpdating) withObject:nil afterDelay:3];
    
    
}

- (void)stopUpdating
{
    [_locationManager stopUpdatingLocation];
    
}


@end
