//
//  main.m
//  CheckIn
//
//  Created by Miniplayground on 5/14/56 BE.
//  Copyright (c) 2556 Fireoneone. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
