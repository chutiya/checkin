//
//  AppDelegate.h
//  CheckIn
//
//  Created by Miniplayground on 5/14/56 BE.
//  Copyright (c) 2556 Fireoneone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
